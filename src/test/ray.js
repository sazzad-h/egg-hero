
    let raysArray = [];
        for(let i = -5; i < 6; i++){
            let ray = this.add.sprite(game.config.width / 2, game.config.height, "ray");
            ray.setOrigin(0.5, 1);
            ray.angle = i * 12;
            ray.displayHeight = game.config.height * 1.2;
            ray.alpha = 0.2;
            raysArray.push(ray);
        }
        this.tweens.add({
            targets: raysArray,
            props: {
                angle: {
                    value: "+= 12"
                }
            },
            duration: 8000,
            repeat: -1
        });