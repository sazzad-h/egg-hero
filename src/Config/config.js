import 'phaser';

export default {
  type: Phaser.AUTO,
  width: 750,
  // height: 1334,
  height: 950,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: 750,
    height: 950
},
backgroundColor: 0x0c88c7,
physics: {
    default: 'arcade',
    arcade: {
        gravity: { y: 300 },
        debug: false
    }
},
};
