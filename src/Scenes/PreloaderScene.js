import 'phaser';

export default class PreloaderScene extends Phaser.Scene {
  constructor () {
    super('Preloader');
  }

  init () {
    this.readyCount = 0;
  }

  preload () {
    // add logo image
    // this.add.image(400, 200, 'logo');

    // display progress bar
    var progressBar = this.add.graphics();
    var progressBox = this.add.graphics();
    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(240, 270, 320, 50);

    var width = this.cameras.main.width;
    var height = this.cameras.main.height;
    var loadingText = this.make.text({
      x: width / 2,
      y: height / 2 - 50,
      text: 'Loading...',
      style: {
        font: '20px monospace',
        fill: '#ffffff'
      }
    });
    loadingText.setOrigin(0.5, 0.5);

    var percentText = this.make.text({
      x: width / 2,
      y: height / 2 - 5,
      text: '0%',
      style: {
        font: '18px monospace',
        fill: '#ffffff'
      }
    });
    percentText.setOrigin(0.5, 0.5);

    var assetText = this.make.text({
      x: width / 2,
      y: height / 2 + 50,
      text: '',
      style: {
        font: '18px monospace',
        fill: '#ffffff'
      }
    });
    assetText.setOrigin(0.5, 0.5);

    // update progress bar
    this.load.on('progress', function (value) {
      percentText.setText(parseInt(value * 100) + '%');
      progressBar.clear();
      progressBar.fillStyle(0xffffff, 1);
      progressBar.fillRect(250, 280, 300 * value, 30);
    });

    // update file progress text
    this.load.on('fileprogress', function (file) {
      assetText.setText('Loading asset: ' + file.key);
    });

    // remove progress bar when complete
    this.load.on('complete', function () {
      progressBar.destroy();
      progressBox.destroy();
      loadingText.destroy();
      percentText.destroy();
      assetText.destroy();
      this.ready();
    }.bind(this));

    this.timedEvent = this.time.delayedCall(3000, this.ready, [], this);

    // load assets needed in our game
    this.load.image('blueButton1', 'assets/ui/blue_button02.png');
    this.load.image('blueButton2', 'assets/ui/blue_button03.png');
    this.load.image('phaserLogo', 'assets/logo.png');
    this.load.image('egLogo', 'assets/eh_logo.png');
    this.load.image('box', 'assets/ui/grey_box.png');
    this.load.image('checkedBox', 'assets/ui/blue_boxCheckmark.png');
    this.load.audio('bgMusic', ['assets/TownTheme.mp3']);

    this.load.audio('bgMusic1', ['assets/Bossa-nova-beat-music-loop.mp3']);
    this.load.audio('bgMusic2', ['assets/Electro-session-ambient-electronic-lounge-music.mp3']);
    this.load.audio('bgMusic3', ['assets/Joy-to-the-world-music-box.mp3']);
    this.load.audio('bgMusic4', ['assets/Short-electronic-background-music.mp3']);

    this.load.audio('chickenSound01', ['assets/sound/Chicken-Sound_01.ogg']);
    this.load.audio('chickenSound02', ['assets/sound/Chicken-Sound_02.ogg']);
    this.load.audio('chickenSound03', ['assets/sound/Chicken-Sound_03.ogg']);
    this.load.audio('chickenSound04', ['assets/sound/Chicken-Sound_04.ogg']);
    this.load.audio('chickenSound1', ['assets/sound/Chicken-Sounds_01.ogg']);
    this.load.audio('chickenSound2', ['assets/sound/Chicken-Sounds_02.ogg']);
    this.load.audio('chickenSound3', ['assets/sound/Chicken-Sounds_03.ogg']);
    this.load.audio('chickenSound4', ['assets/sound/Chicken-Sounds_04.ogg']);
    this.load.audio('chickenSound5', ['assets/sound/Chicken-Sounds_05.ogg']);

    this.load.audio('click1', ['assets/sound/Click_1.ogg']);
    this.load.audio('click2', ['assets/sound/Click_2.ogg']);

    this.load.image("ray", "assets/ray.png");
    this.load.image("sunRay", "assets/sun-ray.png");

    this.load.image("egg-hen", "assets/egg-hen.png");
    this.load.image("love-hen", "assets/love-hen.png");


    this.load.image('blueButton3', 'assets/ui/blue.png');
    this.load.image('blueButton4', 'assets/ui/BlueLight.png');

    this.load.image('purpleButton1', 'assets/ui/Purple.png');
    this.load.image('purpleButton2', 'assets/ui/PurpleLight.png');

    this.load.image('yellowButton1', 'assets/ui/Yellow.png');
    this.load.image('music', 'assets/ui/Music.png');
    this.load.image('music1', 'assets/ui/music1.png');
    this.load.image('music2', 'assets/ui/music2.png');
    this.load.image('play', 'assets/ui/play.png');
    this.load.image('soundOn', 'assets/ui/SoundOn.png');
    this.load.image('sound', 'assets/ui/Sound.png');


    // load images
    this.load.image('background', 'assets/background2.png');
    this.load.image('hen', 'assets/hen.png');
    this.load.image('hen1', 'assets/chicken_1.png');
    this.load.image('hen2', 'assets/chicken_2.png');
    this.load.image('branch', 'assets/branch2.png');
    this.load.image('basket', 'assets/basket.png');
    this.load.image("topground", "assets/topground.png");
    this.load.image("bottomground", "assets/bottomground.png");

    this.load.image("backButton", "assets/ui/arrow.png");
    this.load.image("circleButton", "assets/ui/button_circle.png");
    this.load.image("circleButtonDown", "assets/ui/button_circle_down.png");
    this.load.image("eggIcon", "assets/ui/icon_egg.png");

    this.load.image('egg', 'assets/egg/egg_1.png');
    this.load.image('egg2', 'assets/egg/egg_19.png');
    this.load.image('shit', 'assets/egg/shit.png');
    this.load.image('shitBroken', 'assets/egg/shit_broken.png');
    this.load.image('eggBroken', 'assets/egg/egg_broken.png');

    this.load.image("timebar", "assets/ui/time-bar2.png");
    this.load.image("timebarfill", "assets/ui/Button_loading_line2.png");


    this.load.image("ribon", "assets/ui/ribon2_3.png");
    this.load.image("form", "assets/ui/form_1.png");
    this.load.image("iconHome", "assets/ui/icon_home.png");
    this.load.image("iconRepeat", "assets/ui/icon_repeat.png");
    this.load.image("iconRight", "assets/ui/icon_right.png");


    this.load.image("cloud1", "assets/cloud1.png");
    this.load.image("cloud2", "assets/cloud2.png");
    this.load.image("cloud3", "assets/cloud3.png");
    this.load.image("cloud4", "assets/cloud4.png");
    this.load.image("cloud5", "assets/cloud5.png");
  }

  ready () {
    this.scene.start('Title');
    this.readyCount++;
    if (this.readyCount === 2) {
      this.scene.start('Title');
    }
  }
};
