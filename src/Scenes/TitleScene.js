import 'phaser';
import config from '../Config/config';
import Button from '../Objects/Button';

export default class TitleScene extends Phaser.Scene {
  constructor () {
    super('Title');
  }

  create () {

    this.model = this.sys.game.globals.model;

    this.bgMusic0 = this.sound.add('bgMusic', { volume: 1, loop: true });
    this.bgMusic1 = this.sound.add('bgMusic1', { volume: 1, loop: true });
    this.bgMusic2 = this.sound.add('bgMusic2', { volume: 1, loop: true });
    this.bgMusic3 = this.sound.add('bgMusic3', { volume: 1, loop: true });
    this.bgMusic4 = this.sound.add('bgMusic4', { volume: 1, loop: true });

    this.bgMusic = this.bgMusic4;
    // this.sys.game.globals.bgMusic = this.bgMusic;

    // if (this.model.musicOn === true && this.model.bgMusicPlaying === false) {

    //   // this.bgMusic.play();
    //   this.model.bgMusicPlaying = false;
    // }

    this.click1 = this.sound.add('click1', { volume: 1, loop: false });
    this.click2 = this.sound.add('click2', { volume: 1, loop: false });

    this.ray = this.add.sprite(config.width/2,config.height/2,'sunRay');
    this.ray.alpha = 0.6;
    this.tweens.add({
      targets: this.ray,
      props: {
        rotation: {
          value: "+= 3"
        }
      },
      duration: 18000,
      repeat: -1
    });

    var highEgg = localStorage.getItem("high-egg");
    var totalEgg = localStorage.getItem("total-egg");
    var isMusicOn = localStorage.getItem("is-music-on");
    var isSoundOn = localStorage.getItem("is-sound-on");

    this.model.musicOn = (isMusicOn == 'true');
    this.model.soundOn = (isSoundOn == 'true');

    if(totalEgg==null)
    {
      localStorage.setItem("high-egg", "0");
      localStorage.setItem("total-egg", "0");
      localStorage.setItem("is-music-on",true);
      localStorage.setItem("is-sound-on",true);
      this.model.musicOn = true;
      this.model.soundOn = true;
    }
    console.log(highEgg);



    

    if(this.sys.game.globals.bgMusic == null){
      this.sys.game.globals.bgMusic = this.bgMusic;
    }
    
    if (this.model.musicOn === true && this.model.bgMusicPlaying === true) {
      this.sys.game.globals.bgMusic.volume = 1;
    }
      
    

    console.log("Music - Sound:");
    console.log(this.model.musicOn);
    console.log(this.model.soundOn);

     // this.timedEvent = this.time.addEvent({ delay: 500, callback: rotateRay, callbackScope: this, loop: true });

     var logo = this.add.sprite(config.width/2,160,'egLogo');

     this.config = {
      x: game.config.width / 2,
      y: logo.y+100,
      text: 'Save The Eggs, Be A Hero!',
      style: {
        fontSize: '44px',
        fontFamily: 'Arial',
        wordWrap: { width: 600, useAdvancedWrap: true },
        color: '#fff',
        align: 'center',
        shadow: {
          color: '#45B4E1',
          fill: true,
          offsetX: 2,
          offsetY: 2,
        }
      }
    };
    var logoTitle = this.make.text(this.config).setOrigin( .5,.5);
    logoTitle.setStroke('#45B4E1', 5);

    if(totalEgg>0){
      var text = [
      "Save The Eggs, Be A Hero!",
      "",
      "Highest Saves : "+ highEgg,
      "Total Saves : "+ totalEgg,];
      logoTitle.setText(text);
      logoTitle.y += 60;
    }
    // logoTitle.setShadow(2, 2, '#000', 2, true, false);

    this.egg_hen = this.add.sprite((config.width/2)-40,config.height/2 +70- 20,'egg-hen').setScale(-1.5,1.5);
    this.love_hen = this.add.sprite((config.width/2)+40,config.height/2 +70+ 2,'love-hen').setScale(1.5);

    this.tweens.add({
      targets: this.egg_hen,
      scaleX: -1.8,
      scaleY: 1.8,
      ease: 'Sine.easeInOut',
      duration: 300,
      delay: 50,
      repeat: -1,
      yoyo: true,
      repeatDelay: 500
    });
    this.tweens.add({
      targets: this.love_hen,
      scaleX: 1.8,
      scaleY: 1.8,
      ease: 'Sine.easeInOut',
      duration: 300,
      delay: 60,
      repeat: -1,
      yoyo: true,
      repeatDelay: 500
    });


    // Game
    // this.gameButton = new Button(this, config.width/2, config.height - 200, 'blueButton1', 'blueButton2', 'Play', 'Game');
    this.musicButton =  this.add.sprite(config.width/2 - 200,config.height/2 + 300,'purpleButton1').setScale(.15,0.15).setInteractive();
    this.musicIcon = this.add.sprite(config.width/2 - 200,config.height/2 + 300,'music1').setScale(.8,.8);
    this.setMusicIcon();

    this.musicButton.on('pointerdown', function () {
      this.model.musicOn = !this.model.musicOn;
      if(this.model.soundOn) this.click2.play();
      this.setMusicIcon();
      localStorage.setItem("is-music-on",this.model.musicOn);
    }.bind(this));

    this.musicButton.on('pointerover', function () {
      this.musicButton.setTexture('purpleButton2');
      this.musicButton.setScale(.17,0.17);
      if(this.model.soundOn) this.click1.play();
    }.bind(this));

    this.musicButton.on('pointerout', function () {
      this.musicButton.setTexture('purpleButton1');
      this.musicButton.setScale(.15,0.15);
    }.bind(this));

    this.soundButton =  this.add.sprite(config.width/2 + 200,config.height/2 + 300,'yellowButton1').setScale(.15,0.15).setInteractive();
    this.soundIcon = this.add.sprite(config.width/2 + 200,config.height/2 + 300,'soundOn').setScale(.31,.31);
    this.setSoundIcon();

    this.soundButton.on('pointerdown', function () {
      this.model.soundOn = !this.model.soundOn;
      this.setSoundIcon();
      localStorage.setItem("is-sound-on",this.model.soundOn);
      if(this.model.soundOn) this.click2.play();
    }.bind(this));

    this.soundButton.on('pointerover', function () {
      this.soundButton.setScale(.17,0.17);
      if(this.model.soundOn) this.click1.play();
    }.bind(this));

    this.soundButton.on('pointerout', function () {
      this.soundButton.setScale(.15,0.15);
    }.bind(this));


    this.gameButton =  this.add.sprite(config.width/2, config.height - 200,'blueButton3').setScale(.2,0.2).setInteractive();
    this.add.sprite(config.width/2, config.height - 200,'play').setScale(.45,.45);
    this.gameButton.on('pointerdown', function () {
          // this.scene.stop('Game');
          if(this.model.soundOn) this.click2.play();
          this.scene.start('Game');
        }.bind(this));

    this.gameButton.on('pointerover', function () {
      this.gameButton.setTexture('blueButton4');
      this.gameButton.setScale(.22,0.22);
      if(this.model.soundOn) this.click1.play();
    }.bind(this));

    this.gameButton.on('pointerout', function () {
      this.gameButton.setTexture('blueButton3');
      this.gameButton.setScale(.2,0.2);
    }.bind(this));

    // Options
    // this.optionsButton = new Button(this, config.width/2, config.height/2  + 200, 'blueButton1', 'blueButton2', 'Options', 'Options');

    // // Credits
    // this.creditsButton = new Button(this, config.width/2, config.height/2 + 300, 'blueButton1', 'blueButton2', 'Credits', 'Credits');

    
  }

  setMusicIcon()
  {
    console.log("On Music Change Music - Sound:");
    if (this.model.musicOn === false) {
      this.musicIcon.setTexture('music2');
        this.sys.game.globals.bgMusic.stop();
        // this.bgMusic.stop();
        this.model.bgMusicPlaying = false;
        console.log("Music is off");
      } else 
      {
        if (this.model.bgMusicPlaying === false) {
          this.musicIcon.setTexture('music1');
        this.sys.game.globals.bgMusic.play();
          // this.bgMusic.play();
          this.model.bgMusicPlaying = true;
        }
        console.log("Music is on");
      }

      console.log(this.model.musicOn);
      console.log(this.model.soundOn);

    }
    setSoundIcon()
    {
      console.log("On Sound Change Music - Sound:");
      if (this.model.soundOn === false) {
        this.soundIcon.setTexture('sound');
        console.log("Sound is off");
      } else {
        this.soundIcon.setTexture('soundOn');
        console.log("Sound is on");
      }

      console.log(this.model.musicOn);
      console.log(this.model.soundOn);
    }

    centerButton (gameObject, offset = 0) {
      Phaser.Display.Align.In.Center(
        gameObject,
        this.add.zone(config.width/2, config.height/2 - offset * 100, config.width, config.height)
        );
    }

    centerButtonText (gameText, gameButton) {
      Phaser.Display.Align.In.Center(
        gameText,
        gameButton
        );
    }
  };
