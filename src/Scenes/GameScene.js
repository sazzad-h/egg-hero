import 'phaser';

export default class GameScene extends Phaser.Scene {

  constructor () {
    super('Game');
    this.hens = [];
    this.branch;
    this.basket;
    this.cursors;
    this.speed;
    this.clouds;

  }

  preload () {
  }



  create () {
    this.click2 = this.sound.add('click2', { volume: 1, loop: false });

    this.model = this.sys.game.globals.model;
    if (this.model.musicOn === true && this.model.bgMusicPlaying === true) {
      this.sys.game.globals.bgMusic.volume = 0.5;
    }
    var background = this.add.image(375, 475, 'background');
    background.setDisplaySize(750, 950);

    this.backButton =  this.add.sprite(50, 50,'backButton').setScale(.2,0.2).setInteractive().setDepth(10);
    this.backButton.on('pointerdown', function () {
      // this.scene.start('Title');
      if(this.model.soundOn) this.click2.play();

      this.scene.pause();
      this.scene.launch('UI',{ type: 1 });
      // this.showEnd();
    }.bind(this));

    this.gameTime = 25000;
    this.timeLeft = 0;

    var timebar = this.add.sprite(game.config.width/2, 50, "timebar").setScale(0.8,0.8).setDepth(10);
    this.timebarFill = this.add.sprite(timebar.x,timebar.y,"timebarfill").setScale(0.8,0.8).setDepth(11);
    // this.timebarFill.width =0;
    // this.timebarFill = this.add.sprite(timebar.getTopLeft().x,timebar.getTopLeft().y,"timebarfill").setScale(0.8,0.8).setOrigin( 0,.5);
    // this.timebarMask =  this.add.sprite(timebar.x,timebar.y,"timebarfill").setScale(0.8,0.8);
    // this.timebarMask.visible = false;
    // timebarFill.mask = new Phaser.Display.Masks.BitmapMask(this,this.timebarMask);
    // var tbfw = this.timebarFill.width;
    var tbfsy = this.timebarFill.scaleX;
    this.gameTimer = this.time.addEvent({
      delay: 100,
      callback: function(){
        this.timeLeft += 100;
        // console.log(this.timebarFill.width );
        // let stepWidth = this.timebarMask.displayWidth / 40;
        // this.timebarMask.x = (this.gameTime/this.timeLeft) * this.timebarMask.displayWidth;
        // this.timebarFill.width  = (this.timeLeft/this.gameTime) * tbfw ;
        this.timebarFill.scaleX  = (this.timeLeft/this.gameTime) * 0.8 ;
        if(this.timeLeft >= this.gameTime){
                // this.scene.start("Title")
                // this.showEnd();

                this.scene.pause();
                this.scene.launch('UI',{ type: 2 , collectedEgg: this.collectedEgg });
              }
            },
            callbackScope: this,
            loop: true
          });

    this.add.sprite(game.config.width - 100, 50,'eggIcon').setScale(.2,0.2).setOrigin( .5,.5).setDepth(10);

    // this.text = this.add.text(game.config.width - 50, 50, 'Score : 0', { font: '16px Courier', fill: '#fff' }).setOrigin( .5,.5);
    var scoreX = game.config.width - 50;
    this.config1 = {
      x: scoreX,
      y: 50,
      text: '0',
      style: {
        fontSize: '44px',
        fontFamily: 'Arial',
        color: '#45B4E1',
            // align: 'center',
            // backgroundColor: '#ff00ff',
            shadow: {
              color: '#0c88c7',
              fill: true,
              offsetX: 2,
              offsetY: 2,
            }
          }
        };
        this.text = this.make.text(this.config1).setOrigin( .5,.5);

        this.chickenSound = [];
        this.chickenSound[0] = this.sound.add('chickenSound5', { volume: 0.5, loop: false });
        this.chickenSound[1] = this.sound.add('chickenSound1', { volume: 0.5, loop: false });
        this.chickenSound[2] = this.sound.add('chickenSound2', { volume: 0.5, loop: false });
        this.chickenSound[3] = this.sound.add('chickenSound3', { volume: 0.5, loop: false });
        this.chickenSound[4] = this.sound.add('chickenSound4', { volume: 0.5, loop: false });
        this.chickenSound[5] = this.sound.add('chickenSound01', { volume: 0.5, loop: false });
        this.chickenSound[6] = this.sound.add('chickenSound02', { volume: 0.5, loop: false });
        this.chickenSound[7] = this.sound.add('chickenSound03', { volume: 0.5, loop: false });
        this.chickenSound[8] = this.sound.add('chickenSound04', { volume: 0.5, loop: false });



        this.topGround = this.physics.add.sprite(0, 860, "topground");
        this.topGround.setDisplaySize(game.config.width, 40);
        this.topGround.setOrigin(0, 0)
        this.topGround.setImmovable(true);
        this.topGround.body.allowGravity = false;
        let topGroundBottom = this.topGround.getBounds().bottom;
        this.bottomGround = this.physics.add.sprite(0, topGroundBottom, "bottomground");
        this.bottomGround.setDisplaySize(game.config.width, game.config.height - topGroundBottom);
        this.bottomGround.setOrigin(0, 0);
        this.bottomGround.setImmovable(true);
        this.bottomGround.body.allowGravity = false;

        this.branch = this.physics.add.sprite(400,195,'branch');
        this.branch.setOrigin( .5,.5);
        this.branch.setScale(2.5,.5);
        this.branch.setImmovable(true);
        this.branch.setDepth(10);
        this.branch.body.allowGravity = false;

        this.spawnPoints = [{"x": 75, "y":200},{"x": 225, "y":200},{"x": 375, "y":200},{"x": 525, "y":200},{"x": 675, "y":200}];
        var colors = [0xe1f5fe,0x0d47a1,0xb71c1c];

        this.hens = this.add.group();
        for (var i = this.spawnPoints.length-1; i >= 0; i--) {
          var hen = this.add.sprite(this.spawnPoints[i].x,195,'hen1').setOrigin( 0.5,1).setScale(.8).setDepth(10);

          var j = Phaser.Math.Between(0, 90);
          j = j>60?2:j>30?1:0;
          hen.setTint(colors[j]);
          // hen.body.allowGravity = false;RandomRGB
          // this.hens.push(hen);
          this.hens.add(hen);
          // console.log("Hen is created at "+j*100)
        }
        // console.log("Hen is created as ")
        // console.log(this.hens);

    // var hen = this.physics.add.sprite(400,0,'hen');
    // hen.setOrigin( 0.5,1);
    // hen.setScale(.5);

    

    // this.physics.add.collider(hen, branch);

    this.basket = this.physics.add.sprite(300,860,'basket');
    this.basket.setInteractive();
    this.basket.setImmovable(true);
    this.basket.body.allowGravity = false;
    this.basket.depth = 1;
    this.input.setDraggable(this.basket);
    var self = this;

    this.basket.on('pointerover', function () {
      this.setTint(0xBCAAA4);
      // console.log("Point over basket");
      // console.log(this);
    });
    this.basket.on('pointerout', function () {
      this.basket.clearTint();
    },this);

    this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
      gameObject.x = dragX;
    });

    this.input.on('dragstart', function (pointer, gameObject) {
      gameObject.setTint(0xA1887F);
    });

    this.input.on('dragend', function (pointer, gameObject) {
      gameObject.clearTint();
    });





    this.timedEvent = this.time.addEvent({ delay: 1000, loop: true, callback: this.layEgg ,callbackScope: this,   });

    this.clouds = this.physics.add.group({
      maxSize: 5,
      allowGravity : false,
      createCallback: function (cloud) {
        cloud.setName('cloud' + this.getLength());
        // console.log('Created', cloud.name);
      },
      removeCallback: function (cloud) {
        // console.log('Removed', cloud.name);
      }
    });

    this.clouds.createMultiple({ key: ['cloud1','cloud2','cloud3','cloud4','cloud5'], active: false,visible: false,randomKey: true, max: 5,setScale:
    {
      x: .5,
      y: .5,
    } });

    // var self = this.clouds;
    this.eventCloud = this.time.addEvent({ delay: 1000, loop: true, callback: this.addCloud ,callbackScope: this  });

    // var timedEvent = this.time.addEvent({ delay: 1000,  loop: true });

    // layEgg();

    this.cursors = this.input.keyboard.createCursorKeys();

    this.speed = 1.5;


    // this.eggs = this.physics.add.group({
    //     key: 'egg',
    //     maxSize: 6,
    // });

    // this.physics.add.overlap(this.basket, this.eggs, this.collectEgg, null, this);
    // var egg = this.eggs.get(400,250);
    // egg.setOrigin( 0.5,1);
    // egg.setScale(.5);

    

    this.score =0;
    this.collectedEgg = 0;
    this.eggGravity = 700;


    this.clouds.children.iterate(function (cloud) {
      var distance = Phaser.Math.Between(50, 80);
      var speed = Phaser.Math.GetSpeed(distance, 1);
      cloud.setData('speed',speed );
    },this);


  }

  collectEgg(basket,egg)
  {
    if(egg.isBroken == false){
      // console.log("Collect Egg -->")
      // console.log('Collect Egg Basket is -');
      // console.log(basket);
      egg.disableBody(true, true);
      if(egg.data.values.type== 'normal')
      {
        this.score += 10;
        this.collectedEgg++;
        this.timeLeft -= 200;
        this.text.setText(this.collectedEgg);
        this.eggGravity += 100;
        this.addFlyingText("+.2s",basket.x,basket.y);
      }
      else if(egg.data.values.type== 'blue')
      {
        // this.score += 10;
        this.collectedEgg+= 3;
        this.timeLeft -= 1500;
        this.text.setText(this.collectedEgg);
        this.eggGravity -= 200;
        this.addFlyingText("+3 Egg",basket.x,basket.y);
        this.addFlyingText("+1.5s",basket.x,basket.y,250);
      }
      
    }
  }

  addFlyingText(text,x,y,d=0){
    // console.log("Flyyyyyyy text   - x is "+x+" .y is "+y);
    var config = {
      x: x,
      y: y-50,
      text: text,
      style: {
        fontSize: '36px',
        fontFamily: 'Arial',
        wordWrap: { width: 400, useAdvancedWrap: true },
        color: '#45B4E1',
        align: 'center',
        shadow: {
          color: '#0c88c7',
          fill: true,
          offsetX: 2,
          offsetY: 2,
        }
      }
    };
    var flyingText = this.make.text(config).setOrigin( .5,1);
    var k = Phaser.Math.Between(-200, 200);
    var l = Phaser.Math.Between(200, 400);
    var m = Phaser.Math.Between(-45, 45);
    this.tweens.add({
      targets: flyingText,
      delay: d,
      duration: 1500,
      angle: m,
      x: x+k,
      y: y-l,
      scaleX: 2,
      scaleY: 2,
      ease: 'Sine.easeInOut',
      alpha: 0,
      yoyo: false,
      onCompleteParams : flyingText,
      onComplete: function(){
        flyingText.destroy();
      }
    });
  }

  collectShit(basket,shit)
  {
    if(shit.isBroken == false){
      console.log("Collecting Egg -->")
      shit.disableBody(true, true);
      this.timeLeft += 800;
      this.eggGravity += 150;
    }
  }  

  // breakShit(topGround,shit)
  // {
  //   console.log("Break shit -->")
  //   shit.setTexture('shitBroken').setImmovable(true);
  //   shit.body.allowGravity = false;
  //   shit.isBroken = true;
  //   this.time.addEvent({ delay: 800, callback: function(){
  //     shit.disableBody(true, true);
  //   }, callbackScope: this });
  // }

  // breakEgg(topGround,egg)
  // {
  //   console.log("Break Egg -->")
  //   egg.setTexture('eggBroken').setScale(1).setImmovable(true);
  //   egg.body.allowGravity = false;
  //   egg.isBroken = true;
  //   this.time.addEvent({ delay: 800, callback: function(){
  //     egg.disableBody(true, true);
  //   }, callbackScope: this });
  // }

  layEgg()
  {
    console.log("Hens in LayEgg() ->");

    // console.log("Hens");
    // console.log(this.hens);
    var j = Phaser.Math.Between(0, (this.hens.getLength())*100);
    var k = Phaser.Math.Between(0, 8);
    var l = Phaser.Math.Between(0, 100);

    l = l>90?2:(l>75?1:0);
    j = Math.floor(j/100);
    // console.log("j is "+j + " , Hen length" +this.hens.getLength());
    if(j<0) j=0;
    if(j>=this.hens.getLength()) j = this.hens.getLength()-1;

    // console.log("Hens child");
    // console.log(this.hens.children);

    // console.log("Hens get child");
    // console.log(this.hens.getChildren());
    // console.log(this.hens.getChildren()[j]);

    // console.log("j is "+j+"  ..k is "+k+"  ..l is "+l);

    var hen = this.hens.getChildren()[j];
    if(hen!= null){
      hen.setTexture('hen2');
      this.tweens.add({
        targets: hen,
        scaleX: 0.70,
        scaleY: 0.65,
        ease: 'Sine.easeInOut',
        duration: 100,
        yoyo: true,
        onCompleteParams : hen,
        onComplete: function(){
          hen.setTexture('hen1');
        }
      });
    }
    
    
    if(this.model.soundOn) this.chickenSound[k].play();

    if(l==1){
      var shit = this.physics.add.sprite(hen.x,hen.y+10,'shit').setOrigin( 0.5,1).setScale(1).setGravityY(this.eggGravity);
      shit.isBroken = false;

      var collider = this.physics.add.collider(this.bottomGround, shit,function(topGround,shit)
      {
        console.log("Break shit -->")
        shit.setTexture('shitBroken').setImmovable(true);
        shit.body.allowGravity = false;
        shit.isBroken = true;
        this.physics.world.removeCollider(collider);
        this.time.addEvent({ delay: 800, callback: function(){
          shit.disableBody(true, true);
        }, callbackScope: this,args: [shit] });
      }, null, this);

      this.physics.add.overlap(this.basket, shit, this.collectShit, null, this);
    }
    else{
      var egg;
      if(l==2){
        egg = this.physics.add.sprite(hen.x,hen.y+10,'egg2').setOrigin( 0.5,1).setScale(.5).setGravityY(this.eggGravity);
        egg.isBroken = false;
        egg.setData('type', 'blue');
      }
      else{
        egg = this.physics.add.sprite(hen.x,hen.y+10,'egg').setOrigin( 0.5,1).setScale(.5).setGravityY(this.eggGravity);
        egg.isBroken = false;
        egg.setData('type', 'normal');
      }

      // console.log(egg);
      

      var collider= this.physics.add.collider(this.bottomGround, egg,function(topGround,egg)
      {
        console.log("Break Egg -->")
        egg.setTexture('eggBroken').setScale(1).setImmovable(true);
        egg.body.allowGravity = false;
        egg.isBroken = true;
        // egg.body.stop();
        this.physics.world.removeCollider(collider);
        this.time.addEvent({ delay: 800, callback: function(){
          egg.disableBody(true, true);
        }, callbackScope: this,args: [egg] });
      }, null, this);

      this.physics.add.overlap(this.basket, egg, this.collectEgg, null, this);
    }
  }


  addCloud()
  {
    // console.log('adding cloud');
    // var l = Phaser.Math.Between(1, 5);
    var cloud = this.clouds.get(Phaser.Math.Between(770, 1090), Phaser.Math.Between(100, 400));

    if (!cloud) return;

    cloud
    .setActive(true)
    .setVisible(true);

  }

  update(time, delta)
  {
    // console.log("       Game Scene on UpDate           ");
    this.clouds.children.iterate(function (cloud) {
      cloud.x -= cloud.data.values.speed * delta;
      if (cloud.x < -100 ) {
        this.clouds.killAndHide(cloud);
      }
    },this);

    // Phaser.Actions.IncX(this.clouds.getChildren(), -0.97);

    if (this.cursors.left.isDown)
    {
      this.basket.x -= this.speed * delta;
      // this.basket.setVelocityX(-330);
      this.basket.setTint(0xA1887F);
    }
    else if (this.cursors.right.isDown)
    {
      this.basket.x += this.speed * delta;
      // this.basket.setVelocityX(330);
      this.basket.setTint(0xA1887F);
    }
    else{
      // this.basket.clearTint();
    }
  }

};



