import 'phaser';

export default class UIScene extends Phaser.Scene {

	constructor ()
	{
		super('UI');
	}

	preload ()
	{
	}

    create (data) //type 1 == paused , 2== end
    {

    // this.timedEvent.paused = true;
    // this.gameTimer.paused = true;
    //         // this.scene.pause();
    //         this.time.timeScale = 0;
    if(data.type == 1)
    {
    	var x= 450;
    	var y= 150;
    	var text= "Pasued!"
    }
    else{
    	var x= 550;
    	var y= 250;
    	var text2 = "Saved Eggs : "+ data.collectedEgg;

    	var highEgg = localStorage.getItem("high-egg");
    	var totalEgg = localStorage.getItem("total-egg");

    	totalEgg = Number(totalEgg) + data.collectedEgg;
    	if( data.collectedEgg> highEgg){
    		var text= "New High Saves!!!";
    		localStorage.setItem("high-egg", data.collectedEgg);
    	}
    	else {
    		var text= "Time Finished, Hero!";
    	}
    	localStorage.setItem("total-egg", totalEgg );
    }
    this.model = this.sys.game.globals.model;
    this.click1 = this.sound.add('click1', { volume: 1, loop: false });
    this.click2 = this.sound.add('click2', { volume: 1, loop: false });

    this.msgBox = this.add.group();
      // this.msgBox.create(game.config.width / 2, game.config.height / 2, "form").setScale(.45,.4);
      // this.msgBox.create(game.config.width / 2, game.config.height / 2 - 120, "ribon").setScale(.5,.5);

      var form = this.add.sprite(game.config.width / 2, game.config.height / 2, "form").setDisplaySize(x, y);
      var ribon = this.add.sprite(game.config.width / 2,form.getBounds().top, "ribon");

      this.msgButton1 = this.add.sprite(game.config.width / 2 - 100,form.getBounds().bottom, "circleButton").setScale(.5,.5).setInteractive();
      this.msgButton2 = this.add.sprite(game.config.width / 2 + 100,form.getBounds().bottom, "circleButton").setScale(.5,.5).setInteractive();

      // this.msgBox.create(game.config.width / 2, game.config.height / 2, "form").setDisplaySize(x, y);
      // this.msgBox.create(game.config.width / 2, game.config.height / 2 - 120, "ribon");

      this.config1 = {
      	x: game.config.width / 2,
      	y: game.config.height / 2,
      	text: text,
      	style: {
      		fontSize: '44px',
      		fontFamily: 'Arial',
      		wordWrap: { width: 450, useAdvancedWrap: true },
      		color: '#45B4E1',
      		align: 'center',
      		shadow: {
      			color: '#0c88c7',
      			fill: true,
      			offsetX: 2,
      			offsetY: 2,
      		}
      	}
      };
      this.config2 = {
      	x: game.config.width / 2,
      	y: game.config.height / 2 + 50,
      	text: text2,
      	style: {
      		fontSize: '36px',
      		fontFamily: 'Arial',
      		wordWrap: { width: 400, useAdvancedWrap: true },
      		color: '#45B4E1',
      		align: 'center',
      		shadow: {
      			color: '#0c88c7',
      			fill: true,
      			offsetX: 2,
      			offsetY: 2,
      		}
      	}
      };
      this.msgText = this.make.text(this.config1).setOrigin( .5,.5);
      // this.msgText.setText("Paused!");
      // this.msgText.wordWrapWidth = 100;

      this.msgBox.add(this.msgText);

      this.msgBox.create(this.msgButton1.x, this.msgButton1.y, "iconHome").setScale(.9);
      
      if(data.type == 1)
      {
      	this.msgBox.create(this.msgButton2.x, this.msgButton2.y, "iconRight").setScale(.9);
      }
      else{
      	this.make.text(this.config2).setOrigin( .5,.5);
      	this.msgBox.create(this.msgButton2.x, this.msgButton2.y, "iconRepeat").setScale(.9);
      }

      this.msgBox.add(this.msgButton1);
      this.msgButton1.inputEnabled = true;
      this.msgButton1.on('pointerdown', function () {
      	console.log("On click home button");
      	if(this.model.soundOn) this.click2.play();
      	this.scene.stop();
      	this.scene.stop('Game');
      	this.scene.start('Title');
      	this.msgBox.clear(true, true);
      }.bind(this));


      this.msgButton1.on('pointerover', function () {
      	this.msgButton1.setTexture('circleButtonDown');
      	// this.msgButton1.setScale(.56);
      	if(this.model.soundOn) this.click1.play();
      }.bind(this));

      this.msgButton1.on('pointerout', function () {
      	this.msgButton1.setTexture('circleButton');
      	// this.msgButton1.setScale(.5);
      }.bind(this));

      this.msgBox.add(this.msgButton2);
      this.msgButton2.inputEnabled = true;
      this.msgButton2.on('pointerdown', function () {
      	console.log("On click close button");
      	if(this.model.soundOn) this.click2.play();
      	this.msgBox.clear(true, true);
      	if(data.type == 1)
      	{
      		this.scene.stop('UI');
      		this.scene.resume('Game');
      	}
      	else{
      		// this.scene.stop('UI');
      		this.scene.start('Game');
      	}

      }.bind(this));
      this.msgButton2.on('pointerover', function () {
      	this.msgButton2.setTexture('circleButtonDown');
      	// this.msgButton2.setScale(.56);
      	if(this.model.soundOn) this.click1.play();
      }.bind(this));

      this.msgButton2.on('pointerout', function () {
      	this.msgButton2.setTexture('circleButton');
      	// this.msgButton2.setScale(.5);
      }.bind(this));
  }


  update(time, delta)
  {
  	// console.log("       Scene UIIII on UpDate           ");
  }

  clickHandler ()
  {
        // let sceneB = this.scene.get('MySecondScene');
        // let position = sceneB.getPosition();
        // this.add.image(position.x, position.y, 'asuna');
    }



}
